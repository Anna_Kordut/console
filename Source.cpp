#include "Source.h"
using namespace std;
#include <Windows.h>

namespace assistant {


	static void change_color(int color) {
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
	}
	static void writline(const char* a) {
		cout << a << endl;
	}
	static void writline(int a) {
		cout << a << endl;
	}
	static void writline(double a) {
		cout << a << endl;
	}
	static void write(int a) {
		cout << a;
	}
	static void write(const char* a) {
		cout << a;
	}
	static void write(double a) {
		cout << a;
	}
	static void read(int& a) {
		cout << &a << endl;
		cin >> a;
	}
};
