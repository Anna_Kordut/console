#pragma once
#include <iostream>

namespace assistant {

	class Console {
	public:
		static void change_color(int color);
		static void writline(const char* a);
		static void writline(int a);
		static void writline(double a);
		static void write(const char* a);
		static void write(double a);
		static void read(int& a);
	};
}